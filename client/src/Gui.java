import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;


/**
 * Created by svkreml on 16.12.2016.
 */
public class Gui extends Application implements AutoCloseable {
    private final URI uri = new URI("http://localhost:8080");
    private final CloseableHttpClient client = HttpClients.createDefault();
    Diffy diffy = new Diffy();

    public Gui() throws URISyntaxException {
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        keys(primaryStage);
    }

    void keys(Stage primaryStage) {
        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        Label label = new Label("connecting...");
        gridPane.addRow(0, label);
        Scene scene = new Scene(gridPane);
        primaryStage.setScene(scene);
        primaryStage.show();
        diffy.init(256);
        try {
            HttpPost keyRequest = new HttpPost(uri.resolve("/crypt"));
            keyRequest.setEntity(new StringEntity(JsonUtil.toJson(diffy.getOpenKey())));
            CloseableHttpResponse response = client.execute(keyRequest);
            String reply = EntityUtils.toString(response.getEntity());
            diffy.fInit(JsonUtil.fromJson(reply, BigInteger.class));
            System.out.println(diffy.getK());
            auth(primaryStage);
        } catch (IOException e) {
            label.setText("connection error");
        }


    }

    void auth(Stage primaryStage) {
        GridPane gridPane = new GridPane();
        TextField login = new TextField();
        TextField err = new TextField();
        err.setEditable(false);
        PasswordField password = new PasswordField();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        Button buttonAuth = new Button("Авторизация");
        Button reg = new Button("Регистрация");

        gridPane.addRow(0, new Label("Логин:"), login);
        gridPane.addRow(1, new Label("Пароль:"), password);
        gridPane.addRow(2, buttonAuth, reg, err);
        GridPane.setHgrow(login, Priority.ALWAYS);
        GridPane.setHgrow(password, Priority.ALWAYS);
        GridPane.setHgrow(err, Priority.ALWAYS);


        Scene scene = new Scene(gridPane);
        primaryStage.setScene(scene);
        primaryStage.show();


        buttonAuth.setOnAction(event -> {
            Login l = new Login(login.getText(), password.getText());
            HttpPost request = new HttpPost(uri.resolve("/auth"));
            String json = JsonUtil.toJson(l);
            System.out.println("json = " + json);
            String encryptedJson = Diffy.encrypt(json, diffy.getK());
            System.out.println("encryptedJson = " + encryptedJson);
            try {
                request.setEntity(new StringEntity(encryptedJson));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            try {
                CloseableHttpResponse response = client.execute(request);
                String reply = EntityUtils.toString(response.getEntity());
                String ret = Diffy.decrypt(reply, diffy.getK());
                System.out.println("reply = " + ret);
                err.setText(ret);

                if (ret.equals("OK"))
                    after(primaryStage);
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        reg.setOnAction(event -> {
            Login l = new Login(login.getText(), password.getText());
            HttpPost request = new HttpPost(uri.resolve("/reg"));
            String json = JsonUtil.toJson(l);
            System.out.println("json = " + json);
            String encryptedJson = Diffy.encrypt(json, diffy.getK());
            System.out.println("encryptedJson = " + encryptedJson);
            try {
                request.setEntity(new StringEntity(encryptedJson));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            try {
                CloseableHttpResponse response = client.execute(request);
                String reply = EntityUtils.toString(response.getEntity());
                String ret = Diffy.decrypt(reply, diffy.getK());
                System.out.println("reply = " + ret);
                err.setText(ret);

            } catch (IOException e) {
                e.printStackTrace();
            }

        });

    }

    void after(Stage primaryStage) {
        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        Button spam = new Button("spam");
        Button statistic = new Button("statistic");
        TextArea ans = new TextArea();
        ans.setEditable(false);
        gridPane.addRow(0, spam, statistic);
        gridPane.addRow(1, ans);
        GridPane.setHgrow(spam, Priority.ALWAYS);
        GridPane.setHgrow(ans, Priority.ALWAYS);
        Scene scene = new Scene(gridPane);
        primaryStage.setScene(scene);
        primaryStage.show();


        spam.setOnAction(event -> {
                    HttpPost keyRequest = new HttpPost(uri.resolve("/send"));
                    try {
                        keyRequest.setEntity(new StringEntity(Diffy.encrypt(UUID.randomUUID().toString(), diffy.getK())));
                        CloseableHttpResponse response = client.execute(keyRequest);
                        String reply = null;
                        reply = EntityUtils.toString(response.getEntity());
                        System.out.println("reply = " + reply);
                        System.out.println("diffy = " + diffy.getK());
                        ans.appendText(Diffy.decrypt(reply, diffy.getK())+"\n");
                    } catch (IOException e) {
                        ans.setText(e.getMessage());
                    }
                }
        );
        statistic.setOnAction(event -> {
                    HttpPost keyRequest = new HttpPost(uri.resolve("/statistic"));
                    try {
                        keyRequest.setEntity(new StringEntity(Diffy.encrypt("stat", diffy.getK())));
                        CloseableHttpResponse response = client.execute(keyRequest);
                        String reply = null;
                        reply = EntityUtils.toString(response.getEntity());
                        System.out.println("reply = " + reply);
                        System.out.println("diffy = " + diffy.getK());
                        ans.appendText("\n"+Diffy.decrypt(reply, diffy.getK()));
                    } catch (IOException e) {
                        ans.setText(e.getMessage());
                    }
                }
        );
    }

    @Override
    public void close() throws Exception {

    }
}
