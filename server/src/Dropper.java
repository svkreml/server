import java.util.Date;
import java.util.NoSuchElementException;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by svkreml on 27.10.2016.
 */
public class Dropper {
    Queue<Message> buffer = new ArrayBlockingQueue<Message>(5);
    Thread producerThread;
    Thread consumerThread;
    AtomicLong counter = new AtomicLong();
    AtomicLong counterWrong = new AtomicLong();
    AtomicLong counterDropped = new AtomicLong();
    AtomicLong counterTimeout = new AtomicLong();
    boolean stop;

    public String finish() {
        //stop = false;
        double counterDropped = this.counterDropped.get();
        double counter = this.counter.get();
        double counterWrong = this.counterWrong.get();
        double counterTimeout = this.counterTimeout.get();
        double dropped = counterDropped / counter;
        double wrong = counterWrong / counter;
        double timeout = counterTimeout / counter;
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "sended = " + counter + " wrong = " + wrong + " dropped = " + dropped + " timeout = " + timeout;
    }

    public String get(String msg, String login) {
        Message message = new Message(msg, login);
        try {
            counter.incrementAndGet();
            buffer.add(message);
        } catch (IllegalStateException e) {
            counterDropped.incrementAndGet();
            return "queue full";
        }
        while ((System.currentTimeMillis() - message.time) <= 250) {
            //System.out.println("time = " + (System.currentTimeMillis()-message.time));
            if (message.boo)
                return message.line;
        }
        counterTimeout.incrementAndGet();
        return "timeout";
    }

    public void start() {
        stop = true;
        AtomicLong x = new AtomicLong();
        new Thread(() -> {
            while (true)
                if (x.get() > 0) {
                    System.out.println("timeout");
                    x.decrementAndGet();
                    if (x.get() == 0 && !stop)
                        break;
                }
        }).start();
        consumerThread = new Thread(() -> {

            while (stop) {
                try {
                    try {
                        Thread.sleep(new Random(0).nextInt(300));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Message msg = buffer.remove();
                    if (new Date().getTime() - msg.time > 200) {
                        counterTimeout.incrementAndGet();
                        msg.setLine("timeout1");
                        x.incrementAndGet();
                        msg.boo = true;
                        continue;
                    }
                    if (msg.line.charAt(0) == 'a' || msg.line.charAt(0) == 'b') {
                        msg.setLine("wrong line");
                        counterWrong.incrementAndGet();
                        msg.boo = true;
                        continue;
                    }
                    msg.setLine("good line");
                    msg.boo = true;
                } catch (NoSuchElementException e) {
                    //System.out.println("queue empty");
                }
            }

        });
        consumerThread.start();

    }


}
