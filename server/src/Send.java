import org.eclipse.jetty.util.IO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Created by svkreml on 08.12.2016.
 */
public class Send extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String auth = (String) req.getSession().getAttribute("auth");
        String login =(String) req.getSession().getAttribute("login");
        if(!auth.equals("OK")) {
            resp.getWriter().write("not auth");
            return;
        }



        BigInteger key = (BigInteger) req.getSession().getAttribute("key");
        System.out.println("key = " + key);
        byte[] bytes = IO.readBytes(req.getInputStream());
        String input = new String(bytes, "UTF-8");
        input = Diffy.decrypt(input,key);
        //dropper.get(input);
        resp.getWriter().write(Diffy.encrypt(ServerMain.dropper.get(input, login)+" "+login, key));
    }
}
