import java.util.Date;

/**
 * Created by svkreml on 28.10.2016.
 */
public class Message {
    String line;
    long time;
    String login;
    boolean boo=false;
    public Message(String line, String login) {
        this.line = line;
        this.login = login;
        time = new Date().getTime();
    }

    public void setLine(String line) {
        this.line = line;
    }
}
