/**
 * Created by svkreml on 07.12.2016.
 */

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;


public class ServerMain {
    static public Dropper dropper = new Dropper();
    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);
        ServletContextHandler handler = new ServletContextHandler(ServletContextHandler.SESSIONS);
        handler.setMaxFormContentSize(10000);// ограничить размер запроса
        handler.addServlet(Crypt.class, "/crypt");
        handler.addServlet(Auth.class, "/auth");
        handler.addServlet(Reg.class, "/reg");
        handler.addServlet(Send.class, "/send");
        handler.addServlet(Statistic.class, "/statistic");
        server.setHandler(handler);
        server.start();
        dropper.start();
    }


}


