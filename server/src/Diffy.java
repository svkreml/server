import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Base64;

/**
 * Created by svkreml on 16.11.2016.
 */
public class Diffy {


    private BigInteger g;
    private BigInteger p;
    private BigInteger a;
    int size;
    public BigInteger getA() {
        return A;
    }
    public BigInteger getG() {
        return g;
    }

    BigInteger A;
    BigInteger B;

    public BigInteger getK() {
        return K;
    }

    BigInteger K;

    public OpenKey getOpenKey(){
        return new OpenKey(p,g,A,size);
    }
    public void init(int size){
        this.size = size;
        g = new BigInteger(size, new SecureRandom());
        p = new BigInteger(size, new SecureRandom());
        a = new BigInteger(size, new SecureRandom());
        A = g.modPow(a, p);
    }


    public void init(OpenKey ok){
        size=ok.size;
        p=ok.p;
        g=ok.g;
        B= ok.A;
        a = new BigInteger(size, new SecureRandom());
        A = g.modPow(a, p);
        K = B.modPow(a, p);
    }
    public void fInit(BigInteger B){
        K = B.modPow(a, p);
    }

    static public String encrypt(String input, BigInteger key){
        return Base64.getEncoder().encodeToString(((new BigInteger(input.getBytes(StandardCharsets.UTF_8)))).xor(key).toByteArray());
    }

    static public String decrypt(String input, BigInteger key){
        return new String((new BigInteger(Base64.getDecoder().decode(input))).xor(key).toByteArray(), StandardCharsets.UTF_8);
    }
}

