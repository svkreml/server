import authentication.FileManager;
import org.eclipse.jetty.util.IO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Created by svkreml on 07.12.2016.
 */
public class Auth extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");
        BigInteger key = (BigInteger) req.getSession().getAttribute("key");
        String filename = "data.db";
        FileManager filemanager = new FileManager(filename);
        authentication.Auth db = new authentication.Auth(filemanager);


        if (key == null) {
            resp.sendError(HttpServletResponse.SC_FORBIDDEN);
            return;
        }

        byte[] bytes = IO.readBytes(req.getInputStream());
        String encryptedString = new String(bytes, "UTF-8");
        String decryptedString = Diffy.decrypt(encryptedString, key);
        Login login = JsonUtil.fromJson(decryptedString, Login.class);
        System.out.println("decryptedString = " + decryptedString);
        String reply;
        String l = db.authorization(login.login, login.password);

        if (l.equals(login.login)) {
            req.getSession().setAttribute("auth", "OK");
            req.getSession().setAttribute("login",login.login);
            reply = "OK";
        } else reply = l;
        reply = Diffy.encrypt(reply, key);
        resp.getWriter().write(reply);
    }

}
