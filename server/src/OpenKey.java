import java.math.BigInteger;

/**
 * Created by svkreml on 07.12.2016.
 */
public class OpenKey {
    public OpenKey(BigInteger p, BigInteger g, BigInteger A, int size) {
        this.p = p;
        this.g = g;
        this.A = A;
        this.size = size;
    }

    public OpenKey() {
    }

    BigInteger p,g, A;
    int size;

}
