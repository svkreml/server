import org.eclipse.jetty.util.IO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by svkreml on 07.12.2016.
 */
public class Crypt extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");
        resp.setCharacterEncoding("UTF-8");

        byte[] bytes = IO.readBytes(req.getInputStream());
        String openKey = new String(bytes, "UTF-8");

        Diffy diffy = new Diffy();
        diffy.init(JsonUtil.fromJson(openKey, OpenKey.class));
        resp.getWriter().write(JsonUtil.toJson(diffy.getA()));
        req.getSession().setAttribute("key", diffy.getK());
        System.out.println("diffy = " + diffy.getK());
    }
}
