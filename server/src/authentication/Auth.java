package authentication;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static authentication.Hasher.toSHA1;

/**
 * Created by svkreml on 19.09.2016.
 */
public class Auth {
    Map<String, Hasher> db;
    FileManager filemanager = null;

    public Auth(FileManager filemanager) {
        this.filemanager = filemanager;
        db = (Map<String, Hasher>) filemanager.load();
        if (db == null) db = new HashMap();
    }

    public String findLogin(String login) {
        return db.containsKey(login)?"Login "+login:"not found";
    }

    public Set<String> listLogin() {
        return db.keySet();
    }

    public String list() {
        StringBuilder buff = new StringBuilder();
        buff.append("login                 date           hash\n");
        buff.append("--------------------------------------------------------------\n");
        for (String key : db.keySet()) {
            char[] spaces = new char[(22 - key.length())];
            for (int i = 0; i < (20 - key.length()); i++) {
                spaces[i] = ' ';
            }
            buff.append(key + String.valueOf(spaces) + db.get(key).getDate() + "   " + (db.get(key).getHash()) + '\n');
        }
        return buff.toString();
    }

    public String deleteLogin(String login) {
        if (db.containsKey(login)) {
            db.remove(login);
            filemanager.save(db);
            return login +" deleted";
        }
        return login+ " not found";
    }

    public String registarion(String login, String password) {
        if(login.length()<3) return "login too short";
        if(password.length()<3) return "password too short";
        if (login.length() > 20) return "Login too long";
        if (password.length() > 20) return "Password too long";
        if (db.containsKey(login)) return login + " already exist";
        Date date = new Date();
        Hasher hasher = new Hasher(password, date.getTime());
        db.put(login, hasher);
        filemanager.save(db);
        return "OK";
    }

    public String authorization(String login, String password) {
        if(login.length()<3) return "login error";
        if(password.length()<3) return "password error";
        if (!db.containsKey(login)) return "Login " + login + " not found";
        Hasher hasher = db.get(login);
        if (hasher.getHash().equals(toSHA1(password + hasher.getDate())))
            return login;
        else return "Wrong password";
    }


}

